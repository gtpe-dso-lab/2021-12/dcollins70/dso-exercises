# DevSecOps Lab Exercises

These lab exercises are in support of the [DevSecOps and Military Applications](https://pe.gatech.edu/courses/devsecops-and-military-applications) (DEF 4400P) course through Georgia Tech Professional Education.

To get started there are a couple of options with regard to a development environment - please see: [setup.md](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/blob/main/setup.md) (as [PDF](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/jobs/artifacts/main/file/output/pdf/setup.pdf?job=build_pdf)) for more details.

The exercises reference two provided source projects:

* https://gitlab.com/gtpe-dso-lab/flask_helloworld (exercises 1-3)
* https://gitlab.com/gtpe-dso-lab/pymodule (exercise 4)

## Lab Exercise One

* [Markdown file](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/blob/main/exercise1.md)
* [PDF file](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/jobs/artifacts/main/file/output/pdf/exercise1.pdf?job=build_pdf)

Exercise One focuses on creating a fork and initial execution of the pipeline.

There are a few tasks required to resolve the build and test phase of the pipeline.

## Lab Exercise Two

* [Markdown file](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/blob/main/exercise2.md)
* [PDF file](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/jobs/artifacts/main/file/output/pdf/exercise2.pdf?job=build_pdf)

Exercise Two focuses on the setup of the GitOps configuration and deploying the service to a staging environment.

## Lab Exercise Three

* [Markdown file](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/blob/main/exercise3.md)
* [PDF file](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/jobs/artifacts/main/file/output/pdf/exercise3.pdf?job=build_pdf)

Exercise Three focuses on a suite of functional tests used to verify the service, before deployment to production.

## Lab Exercise Four

* [Markdown file](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/blob/main/exercise4.md)
* [PDF file](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/jobs/artifacts/main/file/output/pdf/exercise4.pdf?job=build_pdf)


Exercise Four dives into the creation of a Python module, showing versioning, release and artifact management.

----

## `dso_exercises` Dev Info

### Spell Check

`Hunspell` is used for spell check. The slides in this repo use technical jargon / acronyms that are not recognized by the general-purpose dictionaries. An additional file, `dictionary.txt`, is used to provide additional "legitimate" words to `hunspell`. As content is created, `dictionary.txt` will need to be updated with any valid words being flagged as misspellings. 

To alphabetically sort `dictionary.txt` run the following
```
python utils/sort.py dictionary.txt
```

## Automated Builds

This repo uses GitLab CI to auto-build the PDFs of the slides. All \*.md files in the root (other than `README.md`) of the repo are converted. 

> Note: CI will fail if `hunspell` finds a misspelled word. See below for how to run spell check locally (before you get to CI). Update `dictionary.txt` with any additional words that are valid spellings (but being flagged as misspellings). 

## Latest PDFs

note: all links are top the main branch. 

The latest PDFs are located [here](https://gitlab.com/gtpe-dso-lab/dso-exercises/-/jobs/artifacts/main/browse/output/pdf?job=build_pdf). 

## Local Usage

### Build all slides as PDF and HTML
```
sh build_local.sh
```
