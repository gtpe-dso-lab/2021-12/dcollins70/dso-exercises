# DSO Course - Lab Exercise Three

## Functional Testing

### Step 1: Update your IP address

Now the app is staged, it is available to run "functional" tests against.  If your pipeline has not run since your staging site was deployed, rerun the pipeline. 

Start by looking at the failed stage in the CI/CD pipeline. 

You'll need to add the staging IP address to the defined functional test, so start looking at the `*_test.robot` source files. Find and correct the IP address. Replace the local IP / port with your staging IP. 

You can find your staging and prod IPs here: https://gitlab.com/gtpe-dso-lab/YYYY-MM/wiki/-/wikis/IPs where MM and YYYY are the month and year of the course.

### Step 2: Fix your functional tests

There are 3 robot framework tests which test the 3 defined endpoints. **What's an endpoint?** Go look in your `helloworld.py` at the defined routes (`@app.route`).

Look at the logs in GitLab and debug the 3 functional tests.

NOTE: Assume the tests are correct. Your code is the problem - not the tests.

## Verify the Release Stage

### Step 3: Deploy to production

One you have a passing functional test, you will be able to push to production. 

Your pipeline will be blocked; waiting for you to designate the type of release you have created -- `major`, `minor`, or `patch`.  Click one of the 3 options in your pipeline and your application will deploy to production. 

You should be able to verify your production instance via the production IP assigned to you.

## Update, Test, and Release

### Step 4: Continuous Deployment

Now go ahead and make a change to one of the endpoints, and the matching test cases.  Verify the functional tests still pass (and manually verify by opening the staging IP in your browser).  Then release and watch for your update to show up at the production IP.