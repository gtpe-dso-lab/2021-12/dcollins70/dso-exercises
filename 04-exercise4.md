# DSO Course - Lab Exercise Four

## Versioning, Artifacts, and Release Management

We have another base project to demonstrate in pretty good detail how to create a pipeline for a Python module that deploys to GitLab's built-in PyPI package repository.

Your mission here is to deploy a python package to the repo's package registry.

### Step 1: Fork `pymodule`

To start, fork `pymodule` (https://gitlab.com/gtpe-dso-lab/pymodule) into the subgroup you created in setup (in the `Create Your Subgroup` section). Do this by selecting the `Fork` [button](https://gitlab.com/gtpe-dso-lab/pymodule/-/forks/new) in the top right corner of the repo. 

### Step 2: Create a personal access token for your new repo

As you did in `exercise 2`, create a [personal access token](https://gitlab.com/-/profile/personal_access_tokens) for your new repo, but this time you'll need to add the `read_api` permission in addition to `read_repository` and `write_repository`. You will use this token in the next step, so go ahead and copy it.

### Step 3: Create your environment variables

Next, within the `pymodule` fork, add the following environment variables to GitLab by going to __Settings > CI-CD > Variables__ (as you did in Exercise 2). 

|Environmental Variable|Description|Location|
|----------------------|-----------|--------|
|`APP_REPO_USER`|Your GitLab username. This is needed so your pipeline can write to your git repo.|gtpe-dso-lab/YYYY-MM/your_username/pymodule|
|`APP_REPO_PASSWORD`|Your personal access token which you created above. This is needed so your pipeline can write to your git repo.|gtpe-dso-lab/YYYY-MM/your_username/pymodule|

### Step 4: Protect your branches

Then you need to protect the `develop` and `main` branch. The variables you set above will only work on protected branches. You do this under __Settings > Repository > Protected Branches__. Use the following settings:

|Branch|Allowed to Merge Setting|Allowed to Push Setting|
|------|------------------------|-----------------------|
| `develop` | Maintainers | Developers & Maintainers |
| `main` | Maintainers | No one |

### Step 5: Fix your pipeline

Now, run the pipeline on the `develop` branch. To do this, click on __CI / CD > Pipelines__ on the left-side navigation bar. From there press `Run Pipeline`. Select the `develop` branch. It will ask you to enter any required variables. You can skip this and press `Run Pipeline` again.

Hmm. See if you can figure out why the pipeline failed. 

__Note:__
You can run the python code from within Gitpod. Use the terminal in the bottom pane. Below are some commands to use:
```
# this will install the dependencies in your requirements file
pip install -r requirements.txt

# this will run pylint (this is the same command used within GitLab CI)
find . -type f -iname "*.py" > /tmp/pylint_files.txt
python -m pylint -f colorized $(< /tmp/pylint_files.txt)

# this will run pytest (wrapped by coverage - a package that tracks test coverage)
coverage run --source=. -m pytest

# this returns the results of the coverage evaluation
coverage report -m
```

### Step 6: Merge to `main` and release

Great, you have a working development pipeline. 

Note: GitLab stores the artifacts created for each build and makes them available from the pipeline view. This means that your python package is being archived, but it isn't accessible as a package. For that we need to deploy to the PyPI registry. This is automated in the pipeline as part of the release process. 

Let's merge your changes to `main`. Create a Merge Request (MR) within GitLab to merge from your fork's `develop` to your fork's `main`.

Review the MR and hit `Merge`. This will kick off a pipeline run of the new `main` branch (with your changes). Be sure you merge to *YOUR* `main` (and not the upstream `main`). 

Once the pipeline gets to the `release` stage it will ask you what type of release this is. Is it a patch, minor, or major release? For the purposes of the lab, just pick one. 

Now the pipeline will auto update the VERSION file with the new version (rolling the number based on the type of release), create the python package, and push to your own PyPI registry. 

You can see the deployed package by going to Packages & Registries on the left sidebar and selecting Package Registry. 

Note the use of the VERSION file - which is a bit more traditional than setting version based on the git tag (as seen in the flask-helloworld app).  We're still using a three-option release structure to store up changes, and provide a manual release function.  It is possible to simply do the release on the merge to the `main` branch.  If you're so inclined take a dig into that.

## Extra time?

### Step 7: BONUS: Install your package

Now that you have deployed your application to a PyPI registry you can install it. 

You will need the project ID for your project. From your project's page (your fork of `pymodule`), on the left sidebar, select __Settings > General__. Your Project ID is listed on this page. 

You can install the package from Gitpod. Use the terminal in the bottom pane.

To get the details of the installation command, go to the left sidebar and select __Packages & Registries > Package Registry__. Now you will see your deployed package. Click on it. 

Under Installation / Pip Command you will see the command specific to your package. You can ignore the `__token__:<your_personal_token>` as your registry is public. Without the token, your command should look like this:

`pip install --extra-index-url https://gitlab.com/api/v4/projects/<your_project_id>/packages/pypi/simple realpython-reader`

Run the command.

Now your package is installed. 

You can see it by running `pip list`. This will show you `realpython-reader` with the appropriate version. 

You can also run it and get the version via this command:

`(cd .. && python -c "import reader; print(reader.__version__)")`

(the `cd ..` is needed so the installed app is imported and not the folder `reader`)


### Step 8: BONUS: Upgrade your package

On the `develop` branch of your `pymodule` fork, make an update to the code. Commit and push. Open an MR. Once your pipeline finishes successfully, merge the MR to main. The pipeline will now start on main. The last step will ask you if this is a major, minor, or patch release. Pick one. 

Note: you can create as many versions of the package as you want (without changes) by rerunning the pipeline (be sure to select `main`). 

This will deploy a new version of the package to the registry. You can install the update via `pip` using the command below.

`pip install --upgrade --extra-index-url https://gitlab.com/api/v4/projects/<your_project_id>/packages/pypi/simple realpython-reader`

Alternatively, you can install by version number using the below command.

`pip install --extra-index-url https://gitlab.com/api/v4/projects/<your_project_id>/packages/pypi/simple realpython-reader==X.Y.Z`

You can verify the correct version is installed by running `pip list`.

Obviously, let us know if you have any questions.

__References:__

https://packaging.python.org/tutorials/packaging-projects/

https://docs.gitlab.com/ee/user/packages/pypi_repository/
